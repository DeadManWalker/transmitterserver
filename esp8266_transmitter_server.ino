#include "TransmitterServer.h"

#define STATUS_LED 2
#define NEXT_LED 10
#define TOUCH 5


void setup() {
  Serial.begin(115200);
  Serial.print("Booted!");
  Serial.println();
  pinMode(STATUS_LED, OUTPUT);
  pinMode(NEXT_LED, OUTPUT);
  pinMode(TOUCH, INPUT);

  TServer.init();

}

void loop() {
  bool stat = digitalRead(TOUCH);
  digitalWrite(STATUS_LED, stat);
  digitalWrite(NEXT_LED, !stat);

  TServer.update();
}
