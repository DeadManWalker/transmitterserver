- Webserver code at CodePen: https://codepen.io/DeadManWalker/pen/yqWYWo?editors=1100
- Tools
    * import_webserver_code.py <zip_file>: prepares the zipped webserver content
      and injects it into the right places in TransmitterServer.cpp 
- External libraries
    * ESP8266WiFi.h
    * ArduinoJson.h
    * SD.h
    * RH_ASK.h
    * RCSwitch.h
    * JsonREST.h (https://bitbucket.org/DeadManWalker/jsonrest/src/master/)
    * NtpTime.h (https://bitbucket.org/DeadManWalker/ntptime/src/master/)
-Notes
    * Webserver CSS code is currently hosted on my public Google Drive
-ArduinoIDE Flash settings
	* Board:"Generic ESP8266 Module"
	* Flash Mode:"DIO"
	* Flash Size:"4M (1M SPIFFS)"
	* Debug Level:"Keine"
	* lwIP Variant:"v2 Lower Memory"
	* Reset Method:"ck"
	* Crystal Frequency:"26 MHz"
	* Flash Frequency:"80 MHz"
	* CPU Frequency:"???"
	* Builtin Led:"2"
	* Upload Speed:"Only Sketch"
	* Programmer:"AVRISP mkll"
-ESP UART Flashing
	GPIO15	GPIO0	GPIO2
	low	low	high
