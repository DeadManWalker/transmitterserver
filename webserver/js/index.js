let container_el = document.getElementById("new-dev-inputs");
let append_before_el = document.getElementById("new-action-command-add");
let add_btn_el = document.querySelector("#new-action-command-add .add");
let remove_btn_el = document.querySelector("#new-action-command-add .remove");
let action_cmd_el = document.getElementsByClassName("new-action-command")[0].cloneNode(true);

add_btn_el.addEventListener("click", (event)=>{
  let clone = action_cmd_el.cloneNode(true);
  let counter = document.getElementsByClassName("new-action-command").length+1;
  clone.childNodes.forEach((node)=>{
    if(node.nodeName === "INPUT"){
      ["name", "placeholder"].forEach((attr)=>{
        let val = node.getAttribute(attr);
        node.setAttribute(attr, val.slice(0, val.length-1)+String(counter));
      });
    }
  });
  container_el.insertBefore(clone, append_before_el);
});

remove_btn_el.addEventListener("click", (event)=>{
  let elements = document.getElementsByClassName("new-action-command");
  if(elements.length > 1){
    elements[elements.length-1].remove();
  }
});