#!/usr/bin/env python3

import sys
import zipfile
import requests
import shutil
from os.path import join
from tempfile import NamedTemporaryFile

CPP_FILE = "src/TransmitterServer.cpp"

REPLACEMENT_TAGS = ("ADDCMD", "ADDDEV", "ADDRC", "LISTDEV", "LIST", "FOOTER", "ALL")


def minifyCss(file):
    URL = "https://cssminifier.com/raw"
    data = {'input': open(file, "rb").read()}
    return requests.post(URL, data=data).text

def minifyJs(file):
    URL = "https://closure-compiler.appspot.com/compile"
    data = {
        "js_code" : open(file, "rb").read(),
        "compilation_level" : "SIMPLE_OPTIMIZATIONS",
        "output_format" : "text",
        "output_info" : "compiled_code"               
    }
    return requests.post(URL, data=data).text
    
def minifyHtmlStr(string):
    URL = "https://html-minifier.com/raw"
    data = {'input': string.encode()}
    return requests.post(URL, data=data).text.replace("\n", "")

def minifyHtml(file):
    return minifyHTMLStr(open(file, "r").read())
    
def extractBetweenTags(string, start_tag, end_tag):
    try:
        s1, s2 = string.split(start_tag, 1)
        t1, t2 = s2.split(end_tag, 1)
    except ValueError:
        sys.exit("Start or end tag not found!\nAboarding...")
    return (s1+t2, t1)

def formatString(string):
    string = string.replace("%", "%%")
    string = string.replace("%%s", "%s")
    return string

def main():
    try:
        zip_file = sys.argv[1]
    except IndexError:
        print("Usage: import_webserver_code.py <zip_file>")
        return


    extract_folder = "webserver"
    print("Extracting %s to %s" %(zip_file, extract_folder))
    with zipfile.ZipFile(zip_file, "r") as zipf:
        zipf.extractall(extract_folder)

    print("Minifying CSS")
    css = minifyCss(join(extract_folder, "css", "style.css"))
    css = formatString(css)
    # Currently storing the css on Google Drive
    css = "<link rel=stylesheet href=https://drive.google.com/uc?export=download&id=1ZHu1JhS8Xwz_tGUtmYDW3OuMHLEUB_lD>"

    print("Minifying JavaScript")
    js = minifyJs(join(extract_folder, "js", "index.js"))
    js = formatString(js)

    print("Minifying HTML")
    with open(join(extract_folder, "index.html"), "r") as file:
        html = file.read()
    
    
    html = formatString(html)
    replacements = [];

    for tag in REPLACEMENT_TAGS[:-1]:
        html, extraction = extractBetweenTags(html, 
                            "<!--PYTHON-REPLACE-%s-->" %tag,
                            "<!--PYTHON-REPLACE-%s-END-->" %tag); 
        replacements.append( minifyHtmlStr(extraction) )

    html = minifyHtmlStr(html)
    html = html.replace("<link rel=stylesheet href=css/style.css>",
        #"<style>%s</style>" %css)
        css)
    html = html.replace("<script src=js/index.js></script>",
        "<script>%s</script>" %js)
    replacements.append(html)

    print("Replacing webserver strings in %s" %CPP_FILE)
    temp_file = NamedTemporaryFile(mode='w', delete=False)
    with open(CPP_FILE, "r") as f,\
     temp_file as temp:
        for line in f:
            temp.write(line)
            linestrip = line.strip()
            for tag, repl in zip(REPLACEMENT_TAGS, replacements):
                open_tag = "/*PYTHON-REPLACE-%s*/" %tag
                close_tag = "/*PYTHON-REPLACE-%s-END*/" %tag
                if linestrip == open_tag:
                    line = f.readline()
                    var, val = line.split("=", 1)
                    temp.write(var+"=R\"%TERMINATION%("+repl+")%TERMINATION%\";\n")
                    while f.readline().strip() != close_tag:
                        pass
                    temp.write(close_tag+"\n")
    shutil.move(temp_file.name, CPP_FILE)

    """
    html, html_replace1 = extractBetweenTags(html, "<!--PYTHON-REPLACE-1-->", "<!--PYTHON-REPLACE-1-END-->")
    html, html_replace2 = extractBetweenTags(html, "<!--PYTHON-REPLACE-2-->", "<!--PYTHON-REPLACE-2-END-->")

    html = minifyHtmlStr(html)
    html = html.replace("<link rel=stylesheet href=css/style.css>",
        "<style>%s</style>" %css)
    html = html.replace("<script src=js/index.js></script>",
        "<script>%s</script>" %js)

    html_replace1 = minifyHtmlStr(html_replace1)
    html_replace2 = minifyHtmlStr(html_replace2)


    print("Replacing webserver string in %s" %CPP_FILE)
    REPLACEMENT_TAGS = (
        ("/*PYTHON-REPLACE-1*/", "/*PYTHON-REPLACE-1-END*/", html),
        ("/*PYTHON-REPLACE-2*/", "/*PYTHON-REPLACE-2-END*/", html_replace1),
        ("/*PYTHON-REPLACE-3*/", "/*PYTHON-REPLACE-3-END*/", html_replace2)
    )
    temp_file = NamedTemporaryFile(mode='w', delete=False)
    with open(CPP_FILE, "r") as f,\
     temp_file as temp:
        for line in f:
            temp.write(line)
            linestrip = line.strip()
            for tag_open, tag_close, replacement in REPLACEMENT_TAGS:
                if linestrip == tag_open:
                    line = f.readline()
                    var, val = line.split("=", 1)
                    temp.write(var+"=R\"%TERMINATION%("+replacement+")%TERMINATION%\";\n")
                    while f.readline().strip() != tag_close:
                        pass
                    temp.write(tag_close+"\n")
    shutil.move(temp_file.name, CPP_FILE)
    """


if __name__ == "__main__":
    main()
