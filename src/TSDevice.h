#ifndef TSDevice_H
#define TSDevice_H


#include <stdlib.h>
#include <cstring>
#include <string>
#include <list>
#define ARDUINOJSON_EMBEDDED_MODE 0
#include <ArduinoJson.h>
#include <SD.h>


class TSDevice{

public:
    std::string id;
    std::string name;
    std::string signature;
    bool is_rcswitch;
    std::vector<std::string> cmd_keys;
    std::vector<std::string> cmd_vals;
    std::vector<std::string> param_keys;
    std::vector<std::string> param_vals;

    TSDevice();

    bool asJson(std::string& json_str) const;
    std::string getFilename() const;
    std::string getFilepath() const;
    void generateId();
    bool save() const;
    bool remove() const;

    static bool init(short chip_select, const std::string& dir="");
    static bool getFromFile(File& file, TSDevice& dev);
    static bool getFromId(const std::string& id, TSDevice& dev);
    static bool getFromJson(const std::string& json_str, TSDevice& dev);
    static void getAll(std::vector<TSDevice>& devices);
    static std::string getFilename(const std::string& id);
    static std::string getFilepath(const std::string& id);
    static bool setDirectory(const std::string& dir);

    static std::string getError();

protected:
    static std::list<TSDevice> tsdevices;
    static std::string directory;
    static std::string error;

};


#endif
