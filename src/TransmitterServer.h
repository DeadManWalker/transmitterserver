#ifndef TransmitterServer_H
#define TransmitterServer_H

#include <stdlib.h>
#include <ESP8266WiFi.h>

#define ARDUINOJSON_EMBEDDED_MODE 0
#include <ArduinoJson.h>
#include <SD.h>
#include <RH_ASK.h>
#include <SPI.h> // Not actually used but needed to compile
#include <RCSwitch.h>
#include <JsonREST.h>
#include <NtpTime.h>
#include "TSDevice.h"
#include "SDLogger.h"


#define FUNK_SEND 4

namespace TS{
    extern WiFiServer webserver;
    extern JsonREST api;
    extern RH_ASK funk;
    extern RCSwitch rcswitch;

    namespace RULES {  
        extern Link BASE;  
        extern Link API;
        extern Link DEV;
        extern Link DEV_ITEM;
        extern Link LOG;
        extern Link LOGS;
    };
}

void onUrlInvalid();
void onUrlBase();
void onUrlAPI();
void onUrlDev();
void onUrlDevAdd();
void onUrlDevItem();
void onUrlDevUpdate();
void onUrlDevDelete();
void onUrlLog();

class TransmitterServer{

public:
    TransmitterServer();
    void init() const;

    void update() const;
    void setPrefillDevice(TSDevice dev);

    static std::string getOnlineFor();
    static std::string getOnlineSince();
    static void addLinkDocs(Response&, uint8_t recursive_back=1);

protected:
    const unsigned int NTP_UPDATE_INTERVAL;
    const unsigned int NTP_RECV_TIMEOUT;
    const unsigned int SD_CHIP_SELECT;
    const unsigned short LOG_LOGFILE_COUNT;
    const unsigned long LOG_LOGFILE_SIZE;
    const std::string NTP_SERVER_NAME, LOG_DIR;


    const char* WIFI_SSID;
    const char* WIFI_PW;

    const std::string DEVICE_DIRECTORY;

    TSDevice prefill_device;

    bool connectToWifi() const;
    void setRules() const;
    void sendClientResponse(WiFiClient& client) const;
    std::string getHTML(bool prefill=false) const;
};

extern TransmitterServer TServer;


#endif
