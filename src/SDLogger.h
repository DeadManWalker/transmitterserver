#ifndef SDLogger_H
#define SDLogger_H

#include <string>
#include <SD.h>
#include <NtpTime.h>

enum LEVEL{
    DEBUG,  // Detailed information, typically of interest only when diagnosing problems.
    INFO,   // Confirmation that things are working as expected.
    WARNING,    // An indication that something unexpected happened, or indicative of some problem in the near future.
    ERROR,  // Due to a more serious problem, the software has not been able to perform some function.
    CRITICAL    // A serious error, indicating that the program itself may be unable to continue running.
};

class SDLogger{
public:
    bool init(short chip_select, const std::string& logdir,
                unsigned short logfile_count, unsigned long logfile_size);
    void log(LEVEL level, const std::string& msg);  
    bool clearLogs(); 
    std::string read(unsigned short back=0) const;
    std::string getError() const;
    void setLogLevel(LEVEL level);
    void useStream(Stream* stream);
    void startNewLog();

    static const std::string LEVEL_STR[LEVEL::CRITICAL+1]; 

protected:
    void write(const std::string& msg);
    std::string findLastLogfile(unsigned short& index) const;
    std::string getFullLogfilePath(unsigned short index) const;

    std::string logfile, logdir, error;
    unsigned short logfile_i, logfile_count;
    unsigned long logfile_size;
    LEVEL log_level; 
    Stream* stream;
};

extern SDLogger Log;

#endif

