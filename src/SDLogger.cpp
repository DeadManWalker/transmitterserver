#include "SDLogger.h"
#include "TSUtility.h"


const std::string SDLogger::LEVEL_STR[LEVEL::CRITICAL+1] = {
    "DEBUG",
    "INFO",
    "WARNING",
    "ERROR",
    "CRITICAL"
};


SDLogger Log;

bool SDLogger::init(short chip_select, const std::string& _logdir,
            unsigned short _logfile_count, unsigned long _logfile_size){
    logdir = _logdir;
    if(logdir.back() != '/')
        logdir.append("/");
    logfile_count = _logfile_count;
    logfile_size = _logfile_size;
    error = "";
    log_level = LEVEL::DEBUG;
    stream = nullptr;

    if(chip_select >= 0){
        if(!SD.begin(chip_select)){
            error = "SDLogger: failed to initialize SD library";
            return false;
        }
    }

    if(!SD.exists(logdir.c_str())){
        if(!SD.mkdir(logdir.c_str())){
            error = "SDLogger: failed to create logging directory '" + logdir + "'";
            return false;
        }
    }
    
    logfile = findLastLogfile(logfile_i);
    if(!SD.exists(logfile.c_str())){
        logfile_i = 0;
        logfile = getFullLogfilePath(logfile_i);
    }
    File file = SD.open(logfile.c_str(), FILE_READ);
    if(!file){
        error = "SDLogger: failed to open log file";
        return false;
    }
    file.close();
    return true;
}

void SDLogger::log(LEVEL level, const std::string& msg){
    if(level < log_level)
        return;
    std::string time_string = "undefined";
    if(NTPTime.isSynced()){
        tm utc = NTPTime.getUTC();
        time_string = TSUtility::formatString(
            "%02d-%02d-%02d %02d:%02d:%02d", 
            utc.tm_mday, utc.tm_mon, utc.tm_year+1900, 
            utc.tm_hour, utc.tm_min, utc.tm_sec);
    }
    write(time_string + " [" + LEVEL_STR[level] + "] " + msg + "\n");
}

bool SDLogger::clearLogs(){
    File sdroot = SD.open(logdir.c_str());
    sdroot.rewindDirectory();
    std::string filename;
    bool success = true;

    while (true) {
        File entry = sdroot.openNextFile();
        if (!entry)
          break;
        if (entry.isDirectory())
          continue;
    
        filename = logdir + entry.name();
        entry.close();
        if(!SD.remove(filename.c_str())){
            error = "SDLogger: failed to remove one or more log files";
            success = false;
        }
    }
    sdroot.close();
    return success;
}

std::string SDLogger::read(unsigned short back) const{
    std::string content, filename;
    short filename_i = logfile_i;
    
    for(unsigned short i=0; i<=back; ++i){    
        filename_i -= i;
        if(filename_i < 0)
            filename_i = logfile_count;
        filename = getFullLogfilePath(filename_i);
        if(!SD.exists(filename.c_str()))
            continue;
        File file = SD.open(filename.c_str(), FILE_READ);
        if(!file)
            continue;
        content += file.readString().c_str();
        file.close();
        content += "\n";
    }
    content.pop_back();
    return content;
}

std::string SDLogger::getError() const{
    return error;
}

void SDLogger::useStream(Stream* _stream){
    stream = _stream;
}

void SDLogger::startNewLog(){
    unsigned short last_logindex, next_logindex;
    std::string last_logfile, next_logfile; 

    last_logfile = findLastLogfile(last_logindex);
    logfile_i = last_logindex < logfile_count ? last_logindex+1 : 0;
    next_logindex = logfile_i < logfile_count ? logfile_i+1 : 0; 
    logfile = getFullLogfilePath(logfile_i); 
    next_logfile = getFullLogfilePath(next_logindex);

    if(SD.exists(logfile.c_str()))
        SD.remove(logfile.c_str());
    if(SD.exists(next_logfile.c_str()))
        SD.remove(next_logfile.c_str());     
}

void SDLogger::setLogLevel(LEVEL level){
    log_level = level;
}
      
void SDLogger::write(const std::string& msg){
    if(stream != nullptr){
        stream->write(logfile.c_str(), logfile.length());
        stream->write(" ", 1);
        stream->write(msg.c_str(), msg.length());
    }
    File file = SD.open(logfile.c_str(), FILE_WRITE);
    if(!file)
        return;
    
    file.write(msg.c_str(), msg.length());
    if(file.size() >= logfile_size)
        startNewLog();
    file.close();
}

std::string SDLogger::findLastLogfile(unsigned short& index) const{
    std::string filename, filename_old;
    short temp_index = 0;
    for(unsigned short i=0; i<=logfile_count; ++i){
        filename_old = filename;
        filename = getFullLogfilePath(i);
        temp_index = i;
        if(!SD.exists(filename.c_str())){            
            temp_index--;
            filename = filename_old;
            break;
        }
    }
    if(temp_index < 0){
        index = logfile_count;
        filename = getFullLogfilePath(index);
    }else{
        index = temp_index;
    }
    return filename;
}

std::string SDLogger::getFullLogfilePath(unsigned short index) const{
    return logdir + TSUtility::intToStr(index) + ".txt";
}

