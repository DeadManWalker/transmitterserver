#include "TransmitterServer.h"
#include "TSUtility.h"


TransmitterServer TServer;


namespace TS {
    WiFiServer webserver(80);
    JsonREST api;
    RH_ASK funk(2000, 0, FUNK_SEND, 0);
    RCSwitch rcswitch = RCSwitch();

    namespace RULES {
        Link BASE = Link("");
        Link API = Link("api", &BASE);
        Link DEV = Link("dev", &API);
        Link DEV_ITEM = Link(
            "%s", &DEV, {"/<device ID>", "/rcswitch", "/generic"}
        );
        Link LOG = Link("log", &API);
        Link LOGS = Link("%s", &LOG, {"/<#log files>"});
    }
}

TransmitterServer::TransmitterServer()
 :WIFI_SSID("jah"), WIFI_PW("d34d-m4n-w4lk3r"), NTP_UPDATE_INTERVAL(60*5),
 NTP_RECV_TIMEOUT(1500), NTP_SERVER_NAME("0.de.pool.ntp.org"),
 DEVICE_DIRECTORY("dev"), LOG_DIR("log"), SD_CHIP_SELECT(16),
 LOG_LOGFILE_COUNT(10), LOG_LOGFILE_SIZE(500){
}

void listAll(){
    Serial.println("---- LIST ALL -----");
    File sdroot = SD.open("log");
    sdroot.rewindDirectory();

    while (true) {
        File entry = sdroot.openNextFile();
        if (!entry)
          break;
        if (entry.isDirectory())
          continue;

        Serial.println(String("=== File: ") + entry.name());
        Serial.println(entry.readString());
        Serial.println("-----");
        entry.close();
    }

    sdroot.close();
}

void TransmitterServer::init() const {
  srand(analogRead(A0)); // Initialize with (hopefully) random seed
  bool log_success, wifi_success, ntptime_success, radio_success, tsdevice_success;
  
  log_success = Log.init(SD_CHIP_SELECT, LOG_DIR, LOG_LOGFILE_COUNT, LOG_LOGFILE_SIZE);
  if(!log_success){
    // Error
    Serial.println("[ERROR] " + String(Log.getError().c_str()));
  }
  Log.useStream(&Serial);
  if(log_success)
    Log.log(LEVEL::INFO, "Booted");

  wifi_success = connectToWifi();
  if(!wifi_success) {
    if(log_success)
        Log.log(LEVEL::CRITICAL, "Failed to connect to Wifi");
  }

  ntptime_success = NTPTime.init(NTP_UPDATE_INTERVAL, NTP_RECV_TIMEOUT, NTP_SERVER_NAME);
  if(!ntptime_success){
    if(log_success)
        Log.log(LEVEL::ERROR, "Failed to initialize NTP time");
  }else{
      while(!NTPTime.isSynced()){
        NTPTime.update();
        delay(50);
      }
  }
  
  TS::funk.setModeTx();
  radio_success = TS::funk.init();
  if(!radio_success) {
    if(log_success)
        Log.log(LEVEL::CRITICAL, "Failed to initialize radio module");
  }

  tsdevice_success = TSDevice::init(SD_CHIP_SELECT, DEVICE_DIRECTORY);
  if(!tsdevice_success){
    if(log_success)
        Log.log(LEVEL::CRITICAL, TSDevice::getError());
  }

  TS::rcswitch.enableTransmit(FUNK_SEND);
  TS::webserver.begin();
  setRules();
  
  if(log_success)
    Log.log(LEVEL::INFO, "Initialized");
}

bool TransmitterServer::connectToWifi() const {
  WiFi.begin(this->WIFI_SSID, this->WIFI_PW);
  const int WIFI_TIMEOUT_S = 15;
  long wifi_timeout_ms = WIFI_TIMEOUT_S*1000;
  long started = millis();
  while (WiFi.status() != WL_CONNECTED) {
    if(millis()-started > wifi_timeout_ms)
        return false;
    delay(300);
  }
  return true;
}

void TransmitterServer::update() const {
  NTPTime.update();
  WiFiClient client = TS::webserver.available();
  if (!client) {
    return;
  }
  // Wait until the client sends some data?prefill=2
  while (!client.available()) {
    delay(1);
  }
  TS::api.handleClient(client);
  client.flush();
  sendClientResponse(client);
}

void TransmitterServer::setPrefillDevice(TSDevice dev){
    this->prefill_device = dev;
}

void TransmitterServer::sendClientResponse(WiFiClient& client) const {
    Response& resp = *(TS::api.getResponse());
    std::string response_type = resp.header;
    resp.header = "HTTP/1.1" + TSUtility::intToStr(resp.code) + " " + 
        resp.code_name + "\r\nAccess-Control-Allow-Origin: *\r\n";

    if(response_type == "ui"){
        resp.header += "Content-Type: html\r\n\r\n"; 
        resp.header += getHTML();
    }else if(response_type == "ui-prefill"){
        resp.header += "Content-Type: html\r\n\r\n"; 
        resp.header += getHTML(true);
    }else{
        // Defaults to API/Json response
        resp.header += "Content-Type: application/json\r\n\r\n"; 
        resp.header +=  TS::api.getResponseJsonStr();
    }

    client.print(resp.header.c_str());
}

std::string TransmitterServer::getHTML(bool prefill) const{
  /*PYTHON-REPLACE-ALL*/
  std::string html_all =R"%TERMINATION%(<!DOCTYPE html><html lang=en><head><meta charset=UTF-8><title>server_transmitter_ui</title><link rel=stylesheet href=https://drive.google.com/uc?export=download&id=1ZHu1JhS8Xwz_tGUtmYDW3OuMHLEUB_lD></head><body><!DOCTYPE html><html><head><link rel=stylesheet href=https://use.fontawesome.com/releases/v5.2.0/css/all.css integrity=sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ crossorigin=anonymous></head><body><main><h1>Server Transmitter</h1><iframe name=dummypage></iframe>%s<hr>%s<hr><h2>Your Devices</h2><div class=container>%s</div><footer>%s</footer></main></body></html><script>var container_el=document.getElementById("new-dev-inputs"),append_before_el=document.getElementById("new-action-command-add"),add_btn_el=document.querySelector("#new-action-command-add .add"),remove_btn_el=document.querySelector("#new-action-command-add .remove"),action_cmd_el=document.getElementsByClassName("new-action-command")[0].cloneNode(!0);
add_btn_el.addEventListener("click",function(a){a=action_cmd_el.cloneNode(!0);var d=document.getElementsByClassName("new-action-command").length+1;a.childNodes.forEach(function(a){"INPUT"===a.nodeName&&["name","placeholder"].forEach(function(b){var c=a.getAttribute(b);a.setAttribute(b,c.slice(0,c.length-1)+String(d))})});container_el.insertBefore(a,append_before_el)});
remove_btn_el.addEventListener("click",function(a){a=document.getElementsByClassName("new-action-command");1<a.length&&a[a.length-1].remove()});
</script></body></html>)%TERMINATION%";
/*PYTHON-REPLACE-ALL-END*/

  /*PYTHON-REPLACE-ADDCMD*/
  std::string html_addcmd =R"%TERMINATION%(<div class="new-action-command flex"><input class=input name=action%s placeholder=Action%s required value=%s><input class=input name=command%s placeholder=Command%s required value=%s></div>)%TERMINATION%";
/*PYTHON-REPLACE-ADDCMD-END*/

  /*PYTHON-REPLACE-ADDDEV*/
  std::string html_adddev =R"%TERMINATION%(<form action=/api/dev/%s method=post target=dummypage enctype=application/x-www-form-urlencoded><h2>%s</h2><div class="container flex"><div id=new-dev-inputs class=input-container><div class="new-name-id flex"><input class=input name=name placeholder=Name required value=%s><input class=input name=sig placeholder=Signature required value=%s></div>%s<div id=new-action-command-add><div class=sep></div><div class="buttons flex"><button type=button class=add><i class="fas fa-plus"></i></button><button type=button class=remove><i class="fas fa-minus"></i></button></div></div></div><div class=right-add-btn>%s<button><i class="fas fa-plus"></i></button></div></div></form>)%TERMINATION%";
/*PYTHON-REPLACE-ADDDEV-END*/

  /*PYTHON-REPLACE-ADDRC*/
  std::string html_addrc =R"%TERMINATION%(<form action=/api/dev/%s method=post target=dummypage enctype=application/x-www-form-urlencoded><h2>%s</h2><div class="container flex"><div id=new-rcswitch-inputs class=input-container><div class="new-name-sig flex"><input class=input name=name placeholder=Name required value=%s><input class=input name=sig placeholder=Signature required value=%s></div><input type=hidden name=rcswitch value=true></div><div class=right-add-btn>%s<button><i class="fas fa-plus"></i></button></div></div></form>)%TERMINATION%";
/*PYTHON-REPLACE-ADDRC-END*/

  /*PYTHON-REPLACE-LISTDEV*/
    std::string html_listdev =R"%TERMINATION%(<button type=submit value=%s name=cmd>%s</button>)%TERMINATION%";
/*PYTHON-REPLACE-LISTDEV-END*/

  /*PYTHON-REPLACE-LIST*/
  std::string html_list =R"%TERMINATION%(<div class=dev-item><label>%s</label><form action=/api/dev/%s target=dummypage enctype=application/x-www-form-urlencoded><div class=dev-item-inputs>%s</div></form><div class="right-add-btn right-modify-btn"><form enctype=application/x-www-form-urlencoded><input name=prefill value=%s type=hidden><button class=modify><i class="fas fa-wrench"></i></button></form><form action=/dev/%s method=post target=dummypage enctype=application/x-www-form-urlencoded><input type=hidden name=method value=delete><button class=remove><i class="fas fa-minus"></i></button></form></div></div>)%TERMINATION%";
/*PYTHON-REPLACE-LIST-END*/

  /*PYTHON-REPLACE-FOOTER*/
  std::string html_footer =R"%TERMINATION%(<span>Free Heap: %s bytes</span><span>Free Flash: %s bytes</span><span>Online for: %s hours</span>)%TERMINATION%";
/*PYTHON-REPLACE-FOOTER-END*/

  std::string html_hiddeninput = "<input type=hidden value=%s name=%s />";

  std::string html_final_adddev, html_final_addrc;
  std::string html_final_list, html_final_footer, html_final_all;

  // AaddDev + AddCmd
  {
    std::string header, name_value, sig_value, hidden_inp, html_cmds, url_resource;
    if(prefill && !this->prefill_device.is_rcswitch){
        header = "Update device " + this->prefill_device.name;
        name_value = this->prefill_device.name;
        sig_value = this->prefill_device.signature;
        hidden_inp = TSUtility::formatString(
            html_hiddeninput, "PUT", "method"
        );
        url_resource = this->prefill_device.id;
        std::string counter;
        for(short i=0; i<this->prefill_device.cmd_keys.size(); ++i){
            counter = TSUtility::intToStr(i+1);
            html_cmds.append( TSUtility::formatString(
                html_addcmd, counter.c_str(), counter.c_str(), 
                this->prefill_device.cmd_keys[i].c_str(), counter.c_str(), 
                counter.c_str(), this->prefill_device.cmd_vals[i].c_str()
            ));
        }
    }else{
        header = "Add a new Device";
        html_cmds = TSUtility::formatString(
            html_addcmd, "1", "1", "", "1", "1", ""
        );
    }
    
    html_final_adddev = TSUtility::formatString(
        html_adddev, url_resource.c_str(), header.c_str(), 
        name_value.c_str(), sig_value.c_str(), html_cmds.c_str(), hidden_inp.c_str());
  }

  // AddRC
  {
    std::string header, name_value, sig_value, hidden_inp, url_resource;
    if(prefill && this->prefill_device.is_rcswitch){
        name_value = this->prefill_device.name;
        sig_value = this->prefill_device.signature;
        header = "Update RC-Switch " + name_value;
        hidden_inp = TSUtility::formatString(html_hiddeninput, "PUT", "method");
        url_resource = this->prefill_device.id;
    }else{
        header = "Add a new RC-Switch";
    }

    html_final_addrc = TSUtility::formatString(
        html_addrc, url_resource.c_str(), header.c_str(), 
        name_value.c_str(), sig_value.c_str(), hidden_inp.c_str()
    );
  }

  // ListDev + List
  {  
    std::vector<TSDevice> devices;
    TSDevice::getAll(devices);
    std::string html_buttons, hidden_inputs, html_buttons_inputs, resource;

    // ListDev
    for(const auto& dev : devices){
        html_buttons = "";
        hidden_inputs = "";

        for(short i=0; i<dev.cmd_keys.size(); ++i){
          html_buttons.append( TSUtility::formatString(
            html_listdev, dev.cmd_vals[i].c_str(), dev.cmd_keys[i].c_str()
          ));
        }
        for(short i=0; i<dev.param_keys.size(); ++i){
          hidden_inputs.append( TSUtility::formatString(
            html_hiddeninput, dev.param_vals[i].c_str(), dev.param_keys[i].c_str()
          ));
        }
        hidden_inputs.append( TSUtility::formatString(
            html_hiddeninput, dev.signature.c_str(), "sig"
        ));
        html_buttons_inputs = html_buttons + hidden_inputs;

        // List
        resource = dev.id.c_str();
        html_final_list.append( TSUtility::formatString(
            html_list, dev.name.c_str(), resource.c_str(), 
            html_buttons_inputs.c_str(), resource.c_str(), resource.c_str()
        ));
    }
  }   

  // Footer
  {
    const std::string freeheap(TSUtility::intToStr(ESP.getFreeHeap()));
    const std::string freeflash(TSUtility::intToStr(ESP.getFlashChipSize()));
    const std::string onlinefor(getOnlineFor());
    html_final_footer = TSUtility::formatString(
        html_footer, freeheap.c_str(), freeflash.c_str(), onlinefor.c_str()
    );
  }

  html_final_all = TSUtility::formatString(
        html_all, html_final_adddev.c_str(), html_final_addrc.c_str(), 
        html_final_list.c_str(), html_final_footer.c_str()
  );
  return html_final_all;
}

std::string TransmitterServer::getOnlineFor(){
    time_t sec_for = millis()/1000;
    tm* timefor = localtime(&sec_for);
    return TSUtility::formatString("%02d:%02d:%02d", timefor->tm_hour, timefor->tm_min,
                        timefor->tm_sec);
}

std::string TransmitterServer::getOnlineSince(){
    if(!NTPTime.isSynced())
        return "[Not Synced]";
    time_t sec_since = NTPTime.getTimestamp() - (millis()/1000);
    tm* timesince = localtime(&sec_since);
    timesince->tm_hour += 2;
    return TSUtility::formatString("%02d-%02d-%02d %02d:%02d:%02d", timesince->tm_mday, 
        timesince->tm_mon, timesince->tm_year+1900, timesince->tm_hour, 
        timesince->tm_min, timesince->tm_sec);
}

void TransmitterServer::addLinkDocs(Response& resp, uint8_t recursive_back){
    std::vector<Link*> links({resp.rule.link});
    std::vector<Link*> next_links;

    for(uint8_t i=0; i<=recursive_back; ++i){
        for(Link* link : links){
            resp.links.push_back(*link);
            next_links.insert(next_links.end(), link->children.begin(), link->children.end());
        }
        links.clear();
        links.assign(next_links.begin(), next_links.end());
        next_links.clear();
    }
}

void TransmitterServer::setRules() const {
  using namespace TS::RULES;
  TS::api.addRules(std::vector<Rule>({
      Rule(&BASE, onUrlBase),
      Rule(&API, onUrlAPI),
      Rule(&DEV, onUrlDev, onUrlDevAdd),
      Rule(&DEV_ITEM, onUrlDevItem, nullptr, onUrlDevUpdate, onUrlDevDelete),
      Rule(&LOG, onUrlLog),
      Rule(&LOGS, onUrlLog)
  }));
  TS::api.createLinkHierarchy();
  TS::api.completeLinkHref();
  TS::api.setInvalidCallback(onUrlInvalid);
}

void onUrlInvalid() {
    // TODO: 404 Not Found page
}
void onUrlBase() {
    Response& resp = *(TS::api.getResponse());

    short prefill_index = TSUtility::findIndexOf(resp.param_keys, "prefill");
    if(prefill_index != -1){
        std::string dev_id = resp.param_vals[prefill_index];
        TSDevice dev; 
        if(TSDevice::getFromId(dev_id, dev)){
            TServer.setPrefillDevice(dev);
            resp.header = "ui-prefill";
            return;
        }
        
    }

    resp.header = "ui";
}
void onUrlAPI() {
    Response& resp = *(TS::api.getResponse());
    TransmitterServer::addLinkDocs(resp);
}
// TODO: Merge with function below, get rid of Json 
void getDeviceIds(std::string& json_str){
    DynamicJsonBuffer buf;
    JsonArray& json_ids = buf.createArray();

    std::vector<TSDevice> devices;
    TSDevice::getAll(devices);
    for(const auto& dev : devices){
        json_ids.add(dev.id);
    } 

    json_ids.printTo(json_str);
}
void onUrlDev() {
  Response& resp = *(TS::api.getResponse());
  TransmitterServer::addLinkDocs(resp);
  getDeviceIds(resp.data);
}

void onUrlDevAdd() {
    Response& resp = *(TS::api.getResponse());
    TransmitterServer::addLinkDocs(resp);
    TSDevice new_dev;

    static const std::string ACTION = "action";
    static const std::string COMMAND = "command";
 
    short name_index = TSUtility::findIndexOf(resp.post_keys, "name");
    if(name_index == -1){
        resp.error(422, "Unprocessable Entity", "Missing required post key 'name' to add a device");
        return;
    }

    short sig_index = TSUtility::findIndexOf(resp.post_keys, "sig");
    if(sig_index == -1){
        resp.error(422, "Unprocessable Entity", "Missing required post key 'signature' to add a device");
        return;
    }

    short rcswitch_index = TSUtility::findIndexOf(resp.post_keys, "rcswitch");
    if(rcswitch_index != -1 && resp.post_vals[rcswitch_index] == "true"){
        new_dev.is_rcswitch = true;
        new_dev.cmd_keys = std::vector<std::string>({"On", "Off"});
        new_dev.cmd_vals = std::vector<std::string>({"1", "0"});
    }else{
        for(short i=0; i<resp.post_keys.size(); ++i){
            if(resp.post_keys[i].substr(0, ACTION.length()) == ACTION){
                new_dev.cmd_keys.push_back(resp.post_vals[i]);
            }else if(resp.post_keys[i].substr(0, COMMAND.length()) == COMMAND){
                new_dev.cmd_vals.push_back(resp.post_vals[i]);
            }
        }
        new_dev.is_rcswitch = false;
    }
    new_dev.name = resp.post_vals[name_index];
    new_dev.signature = resp.post_vals[sig_index];
    new_dev.generateId();

    if(! new_dev.save()){
        resp.error(505, "Internal Server Error", "Failed to save device");
        Log.log(LEVEL::ERROR, TSDevice::getError());
    }

    resp.success(201, "Created", "Device has been created and added");
    resp.data = "{\"device\": " + new_dev.id + "}";
}
void onUrlDevUpdate() {
    Response& resp = *(TS::api.getResponse());
    TransmitterServer::addLinkDocs(resp);
    std::string resource = resp.args[0];
    TSDevice dev;

    static const std::string ACTION = "action";
    static const std::string COMMAND = "command";

    if(!TSDevice::getFromId(resource, dev)){ 
        resp.error(404, "Not Found", "Invalid resource '" + resource + "'");
        return;
    }

    short name_index = TSUtility::findIndexOf(resp.post_keys, "name");
    if(name_index != -1)
        dev.name = resp.post_vals[name_index];
    short sig_index = TSUtility::findIndexOf(resp.post_keys, "signature");
    if(sig_index != -1)
        dev.signature = resp.post_vals[sig_index];

    if(!dev.is_rcswitch){
        dev.cmd_keys.clear();
        dev.cmd_vals.clear();
        for(short i=0; i<resp.post_keys.size(); ++i){
            if(resp.post_keys[i].substr(0, ACTION.length()) == ACTION){
                dev.cmd_keys.push_back(resp.post_vals[i]);
            }else if(resp.post_keys[i].substr(0, COMMAND.length()) == COMMAND){
                dev.cmd_vals.push_back(resp.post_vals[i]);
            }
        }
    }
    
    dev.remove();
    dev.save();
        
}
void onUrlDevDelete() {
    Response& resp = *(TS::api.getResponse());
    TransmitterServer::addLinkDocs(resp);
    std::string resource = resp.args[0];
    
    TSDevice dev;
    if(!TSDevice::getFromId(resource, dev)){
        resp.error(422, "Unprocessable Entity", "Device does not exist");
        return;
    }
    if(!dev.remove()){
        resp.error(422, "Unprocessable Entity", "Device does not exist or failed to get deleted");
        return;
    }
    resp.success(204, "No Content", "Device has been deleted");
}

void handleRCSwitch(Response& resp, TSDevice& dev) {
  std::string cmd;
  short ind_cmd = TSUtility::findIndexOf(resp.param_keys, "cmd");
  // If 'cmd' is missing in url parameters
  if(ind_cmd == -1){
    // And if device is anonymous (not registered), abort
    if(!dev.id.length()){
        resp.error(422, "Unprocessable Entity", "Missing required parameter 'cmd' for anonymous rcswitch");
        return;
    }
    // Else it's a registered device so just return device info
    dev.asJson(resp.data);
    return;
  }
  // Get the command
  cmd = resp.param_vals[ind_cmd];

  short ind_cmd2 = TSUtility::findIndexOf(dev.cmd_keys, cmd);
  // Unless the command is present in the device key, it's another
  // key to a command, so let's get the actual command (value)
  if(ind_cmd2 != -1){
    cmd = dev.cmd_vals[ind_cmd2];
  }

  std::string sig;
  short ind_sig = TSUtility::findIndexOf(resp.param_keys, "sig");
  // If 'sig' is missing in url parameters
  if (ind_sig == -1) {
    // And if device is anonymous (not registered), abort
    if(!dev.id.length()){
        resp.error(422, "Unprocessable Entity", "Missing required parameter 'sig' for rcswitch signature");
        return;
    }
    // Else take the device 'sig' value
    sig = dev.signature;
  }else{
    // Else take the url 'sig' value
    sig = resp.param_vals[ind_sig];
  }

  // Signature validation
  if (sig.length() != 10) {
    resp.error(422, "Unprocessable Entity", "Parameter 'sig' needs to be of length 10 consisting of '1|0'");
    return;
  }

  std::string cmd_upper = cmd;
  TSUtility::toUpperCase(cmd_upper);
  std::string sig1 = sig.substr(0, 5);
  std::string sig2 = sig.substr(5, 10);
  bool succ;
  if (cmd == "ON" || cmd == "1") {
    TS::rcswitch.switchOn(sig1.c_str(), sig2.c_str());
    resp.data = "\"" + dev.name + ": on\"";
  } else if (cmd == "OFF" || cmd == "0") {
    TS::rcswitch.switchOff(sig1.c_str(), sig2.c_str());
    resp.data = "\"" + dev.name + ": off\"";
  } else {
    resp.error(422, "Unprocessable Entity", "Parameter needs to be of cmd=<ON|1|OFF|0>");
  }
}
void handleFunk(Response& resp, TSDevice& dev) {
  std::string cmd;
  short ind_cmd = TSUtility::findIndexOf(resp.param_keys, "cmd");
  // If 'cmd' is missing in url parameters
  if (ind_cmd == -1) {
    // And if device is anonymous (not registered), abort
    if(!dev.id.length()){
        resp.error(422, "Unprocessable Entity", "Missing required parameter 'cmd' for anonymous device");
        return;
    }
    // Else it's a registered device so just return device info
    dev.asJson(resp.data);
    return;
  }
  // Get the command
  cmd = resp.param_vals[ind_cmd];

  short ind_cmd2 = TSUtility::findIndexOf(dev.cmd_keys, cmd);
  // Unless the command is present in the device parameters, it's another
  // key to a command, so let's get the actual command (value)
  if(ind_cmd2 != -1){
    cmd = dev.cmd_vals[ind_cmd2];
  }
  // Command validation
  if(cmd.length() == 0){
    resp.error(422, "Unprocessable Entity", "Parameter 'cmd' must not be empty");
    return;
  }

  std::string sig;
  short ind_sig = TSUtility::findIndexOf(resp.param_keys, "sig");
  // If 'sig' is missing in url parameters
  if (ind_sig == -1) {
    // And if device is anonymous, abort
    if(!dev.signature.length()){
        resp.error(422, "Unprocessable Entity", "Missing required parameter 'sig' for anonymous device signature");
        return;
    }
    // Else take the device 'sig' value
    sig = dev.signature;
  }else{
    // Else take the url 'sig' value
    sig = resp.param_vals[ind_sig];
  }
  
  // Combine the signature (padded to 10 chars) + the command
  static const uint8_t SIG_LENGTH = 10;
  std::vector<uint8_t> funk_cmd(SIG_LENGTH + cmd.length(), 0); // Initialize with 0 

  for(short i=0; i<sig.length() && i<SIG_LENGTH; ++i)
    funk_cmd.at(i) = sig[i];
    Serial.println();
  for(short i=0; i<cmd.length(); ++i)
    funk_cmd.at(i+SIG_LENGTH) = cmd[i];

  if (TS::funk.send(&funk_cmd[0], funk_cmd.size())) {
    TS::funk.waitPacketSent();
    resp.data = dev.name + ": transmitted";
  } else {
    resp.error(500, "Internal Server Error", "Server failed to transmit radio data");
    Log.log(LEVEL::ERROR, "Server failed to transmit radio data");
  }
}
void onUrlDevItem() {
  Response& resp = *(TS::api.getResponse());
  TransmitterServer::addLinkDocs(resp);

  TSDevice dev;
  std::string resource = resp.args[0];
  // Request as anonymous RC-Switch
  if (resource == "rcswitch") {
    dev.name = "Anonymous";
    dev.is_rcswitch = true;
    handleRCSwitch(resp, dev);
    return;
  }
  // Request as anonymous generic device
  if (resource == "generic") {
    dev.name = "Anonymous";
    dev.is_rcswitch = false;
    handleFunk(resp, dev);
    return;
  }
  // Neither RC-Switch nor generic device nor registered device, abort
  if(! TSDevice::getFromId(resource, dev)){
    resp.error(404, "Not Found", "Invalid resource '" + resource + "'");
    return;
  }
  // Registered RC-Switch
  if(dev.is_rcswitch){
    handleRCSwitch(resp, dev);
    return;
  }
  // Registered generic device
  handleFunk(resp, dev);
}

void onUrlLog(){
    Response& resp = *(TS::api.getResponse());
    TransmitterServer::addLinkDocs(resp);
    uint8_t read_past = 0;
    if(resp.args.size() >= 1){
        read_past = atoi(resp.args[0].c_str());
    }

    std::vector<std::string> parts;
    TSUtility::splitString(Log.read(read_past), "\n", parts);
    std::string json_str = "[";
    for(const auto& i : parts){
        json_str += "\"";
        json_str += i;
        json_str += "\",";
    }
    json_str.pop_back();
    json_str += "]";
    Serial.println(json_str.c_str());
    resp.data = json_str;
}

