#include "TSDevice.h"

std::string TSDevice::directory = "Set on init";
std::string TSDevice::error = "";
std::list<TSDevice> TSDevice::tsdevices = {};

TSDevice::TSDevice()
    :id(""), name(""), signature(""), is_rcswitch(false), cmd_keys({}), 
    cmd_vals({}), param_keys({}), param_vals({}){
}

std::string TSDevice::getFilename() const{
    return TSDevice::getFilename(id);
}
std::string TSDevice::getFilepath() const{
    return TSDevice::getFilepath(id);
}

bool TSDevice::asJson(std::string& json_str) const{
    std::string filepath = getFilepath();

    if (!SD.exists(filepath.c_str())) {
        TSDevice::error = "TSDevice: file '" + filepath + "' does not exist";
        return false;
    }
    File file = SD.open(filepath.c_str());
    if (!file) {
        TSDevice::error = "TSDevice: file to open file '" + filepath + "'";
        return false;
    }
    json_str = file.readString().c_str();
    file.close();
    return true;
}

void TSDevice::generateId() {
    static const uint8_t ID_SIZE = 6;
    static const char ID_CHARS[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    static uint8_t id_chars_len = strlen(ID_CHARS);
    char new_id[ID_SIZE+1] = "";

    for(uint8_t i=0; i<ID_SIZE; ++i){
        new_id[i] = ID_CHARS[ rand()%id_chars_len ];
    }
    this->id = std::string(new_id);
}

bool TSDevice::save() const{
    for(const auto& d : TSDevice::tsdevices){
        if(d.id == id){
            remove();
            return save();
        }
    }

    DynamicJsonBuffer buf;
    JsonObject& jsondev = buf.createObject();   

    jsondev["id"] = this->id;
    jsondev["name"] = this->name;
    jsondev["signature"] = this->signature;
    jsondev["is_rcswitch"] = this->is_rcswitch;
    JsonArray& json_cmd_keys = jsondev.createNestedArray("cmd_keys");
    JsonArray& json_cmd_vals = jsondev.createNestedArray("cmd_vals");
    JsonArray& json_param_keys = jsondev.createNestedArray("param_keys");
    JsonArray& json_param_vals = jsondev.createNestedArray("param_vals");

    for(const auto& val : this->cmd_keys)
        json_cmd_keys.add(val);
    for(const auto& val : this->cmd_vals)
        json_cmd_vals.add(val);
    for(const auto& val : this->param_keys)
        json_param_keys.add(val);
    for(const auto& val : this->param_vals)
        json_param_vals.add(val);

    std::string filepath = getFilepath();
    if (SD.exists(filepath.c_str())) {
        TSDevice::error = "TSDevice: file '" + filepath + "' already exists"; 
        return false;
    }
    File newfile = SD.open(filepath.c_str(), FILE_WRITE);
    if(!newfile){
        TSDevice::error = "TSDevice: failed to open file '" + filepath + "'";
        return false;
    }
    std::string json_content;
    jsondev.printTo(json_content); 
    newfile.write(json_content.c_str(), json_content.length());
    newfile.close();

    TSDevice::tsdevices.push_back(*this);

    return true;
}

bool TSDevice::remove() const{
    std::string filepath = getFilepath();
    auto& devs = TSDevice::tsdevices;
    for(auto it = devs.begin(); it != devs.end(); ++it){
        if((*it).id == id){
            devs.erase(it);
            if(! SD.exists(filepath.c_str())){
                TSDevice::error = "TSDevice: file '" + filepath + "' does not exist";
                return false;
            }
            if(! SD.remove(filepath.c_str())){
                TSDevice::error = "TSDevice: failed to remove file '" + filepath + "'";
                return false;
            }
            return true;
        }
    }
    TSDevice::error = "TSDevice: cannot find file '" + filepath + "'";
    return false; 
}


bool TSDevice::init(short chip_select, const std::string& dir) {
    if(chip_select >= 0){
        if(!SD.begin(chip_select)){
            TSDevice::error = "TSDevice: failed to initialize SD library";
            return false;
        }
    }
    return TSDevice::setDirectory(dir);
}

bool TSDevice::getFromFile(File& file, TSDevice& dev) {
    return TSDevice::getFromJson(file.readString().c_str(), dev);
}

bool TSDevice::getFromId(const std::string& id, TSDevice& dev) {
    for(const auto d : TSDevice::tsdevices){
        if(d.id == id){
            dev = d;
            return true;
        }
    }
    TSDevice::error = "TSDevice: cannot find device with id '" + id + "'";
    return false;
}

bool TSDevice::getFromJson(const std::string& json_str, TSDevice& dev) {
    DynamicJsonBuffer buf;
    JsonObject& jsondev = buf.parseObject(json_str);
    if (!jsondev.success()) {
      TSDevice::error = "TSDevice: failed to parse json string";
      return false;
    }    

    dev.id = jsondev["id"].as<const char*>();
    dev.name = jsondev["name"].as<const char*>();
    dev.signature = jsondev["signature"].as<const char*>();
    dev.is_rcswitch = jsondev["is_rcswitch"].as<bool>();

    JsonArray& json_cmd_keys = jsondev["cmd_keys"];
    JsonArray& json_cmd_vals = jsondev["cmd_vals"];
    JsonArray& json_param_keys = jsondev["param_keys"];
    JsonArray& json_param_vals = jsondev["param_vals"];
    if(!json_cmd_keys.success() || !json_cmd_vals.success()
      || !json_param_keys.success() || !json_param_vals.success())
      return false;

    for(auto& val : json_cmd_keys)
      dev.cmd_keys.push_back(val.as<const char*>());
    for(auto& val : json_cmd_vals)
      dev.cmd_vals.push_back(val.as<const char*>());
    for(auto& val : json_param_keys)
      dev.param_keys.push_back(val.as<const char*>());
    for(auto& val : json_param_vals)
      dev.param_vals.push_back(val.as<const char*>());

    return true;

}

void TSDevice::getAll(std::vector<TSDevice>& devices) {
    if(!TSDevice::tsdevices.empty()){
        devices.assign(TSDevice::tsdevices.begin(), TSDevice::tsdevices.end());
        return;
    }

    File sdroot = SD.open(TSDevice::directory.c_str());
    sdroot.rewindDirectory();

    while (true) {
        File entry = sdroot.openNextFile();
        if (!entry)
          break;
        if (entry.isDirectory())
          continue;

        TSDevice dev;
        if (TSDevice::getFromFile(entry, dev)) {
          devices.push_back(dev);
        }
        entry.close();
    }

    sdroot.close();
}

bool TSDevice::setDirectory(const std::string& dir){
    std::string dir_slash = dir;
    if(dir_slash.length() == 0 || dir_slash.back() != '/')
        dir_slash.append("/");
    if(dir_slash == TSDevice::directory)
        return true;

    bool success = true;
    if(!SD.exists(dir.c_str()))
         success = SD.mkdir(dir.c_str());
    if(!success){
        TSDevice::error = "TSDevice: failed to set directory";
        return false;
    }
    TSDevice::directory = dir_slash;
    TSDevice::tsdevices.clear();  
    std::vector<TSDevice> devs;
    TSDevice::getAll(devs);
    TSDevice::tsdevices.assign(devs.begin(), devs.end());
    return true;
}
std::string TSDevice::getFilename(const std::string& id) {
    return id + ".txt";
}
std::string TSDevice::getFilepath(const std::string& id) {
    return TSDevice::directory + TSDevice::getFilename(id);
}

std::string TSDevice::getError(){
    return error;
}
