#ifndef TSUtility_H
#define TSUtility_H


#include <stdio.h>
#include <sstream>
#include <string>
#include <vector>


namespace TSUtility{

    template< class ...Args >
    inline std::string formatString( const std::string& f, Args && ...args ) {
        int size = snprintf( nullptr, 0, f.c_str(), args... );
        std::string res;
        res.resize( size );
        snprintf( & res[ 0 ], size + 1, f.c_str(), args... );
        return res;
    }

    template< class ...Args >
    inline std::string formatString( const char * f, Args && ...args ) {
        int size = snprintf( nullptr, 0, f, args... );
        std::string res;
        res.resize( size );
        snprintf( & res[ 0 ], size + 1, f, args... );
        return res;
    }

    inline std::string intToStr(int x){
        int size = snprintf(nullptr, 0, "%i", x);
        std::string str_num;
        str_num.resize(size);
        snprintf(&str_num[0], size+1, "%i", x);
        return str_num;
    }

    inline short findIndexOf(const std::vector<std::string>& vec, const std::string& str) {
      for (short i = 0; i < vec.size(); ++i) {
        if (vec[i] == str)
          return i;
      }
      return -1;
    }

    inline void toUpperCase(std::string& str){
        for(auto& c: str) 
            c = toupper(c);
    }

    inline void splitString(const std::string& str, const std::string& delim, 
                            std::vector<std::string>& parts, short max_split=0){
        char* pch;
        short index = 0;
        const char* cdelim = delim.c_str();
        std::string _str = str;
        char* cstr = &_str[0u];
        pch = strtok(cstr, cdelim);

        while(pch != NULL){
            parts.push_back(std::string(pch));
            pch = strtok(NULL, cdelim);
            max_split--;
            if(max_split == 0)
                break;
        }
    }
};


#endif
